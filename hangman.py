#Menghubungkan dengan gspread
import csv
data1=[]
data2=[]

csv_file=open('data_user.csv','r')
csv_read=csv.reader(csv_file,delimiter=',')
data=[]
for i in csv_read :
    data1.append(i)
csv_file.close()

csv_file=open('history.csv','r')
csv_read=csv.reader(csv_file,delimiter=',')
data=[]
for i in csv_read :
    data2.append(i)
csv_file.close()

#Hangman
def garis(banyak) :
  if(0==banyak) :
    print("""——————
|
|
|
|
|
|""")
  elif(1 ==banyak) :
    print("""——————
|    |
|
|
|
|
|""")
  elif(2==banyak) :
    print("""——————
|    |
|    o
|
|
|
|""")
  elif(3==banyak) :
    print("""——————
|    |
|    o
|    |
|
|
|""")
  
  elif(4==banyak) :
    print("""——————
|    |
|    o
|    |—
|   
|
|""")
  elif(5==banyak) :
    print("""——————
|    |
|    o
|   —|—
|   
|
|""")
  elif(6==banyak) :
    print("""——————
|    |
|    o
|   —|—
|    |  
|
|""")
  elif(7==banyak) :
    print("""——————
|    |
|    o
|   —|—
|    |  
|   / 
|""")
  elif(8==banyak) :
    print("""——————
|    |
|    o
|   —|—
|    |  
|   / \\
""")

#Array berisi kata-kata tebakan

import random  
kata0=['PAYAKUMBUH','JAKARTA','KUTAI KARTANEGARA','BANDUNG','TEBING TINGGI',
'PANGKAL PINANG','TANJUNG PINANG','BANDA ACEH','BANDAR LAMPUNG','JAKARTA BARAT',
'JAKARTA SELATAN','JAKARTA TIMUR','JAKARTA UTARA','JAKARTA PUSAT','TANGERANG SELATAN'] # berisi nama nama geografis

kata1=['KUPU-KUPU','KUNANG-KUNANG','BADAK BERCULA SATU','KURA-KURA','LABA-LABA',
'LUMBA-LUMBA','BERANG-BERANG','UBUR-UBUR','BERUANG KUTUB','SAPU-SAPU','ALAP-ALAP',
'CUMI-CUMI','UNDUR-UNDUR'] # berisi nama nama hewan

kata2=['KAMBING HITAM','MEJA HIJAU','BANTING TULANG','NAIK PITAM','TANGAN KANAN',
'BESAR KEPALA','KERAS KEPALA','ANAK BUAH','ANAK EMAS','ANGKAT TANGAN','ANGKAT BICARA',
'KUTU BUKU','MUKA TEBAL','MUKA TEMBOK','MUKA DUA','PATAH HATI','PANJANG TANGAN',
'GULUNG TIKAR','BERAT HATI','BUAH TANGAN','RENDAH HATI','BUAH HATI'] # berisi nama nama ungkapan

kata3=['HALIM PERDANAKUSUMA','SULTAN ISKANDAR MUDA','SOEKARNO-HATTA','HUSEIN SASTRANEGARA',
'NGURAH RAI','AHMAD YANI','SULTAN HASANUDDIN','HUSEIN SASTRANEGARA','SYAMSUDDIN NOOR',
'SULTAN THAHA','SAM RATULANGI'] # berisi nama nama bandara

kata4=['TOLONG MENOLONG','BAHU MEMBAHU','TARIK MENARIK','PUKUL MEMUKUL','MONDAR-MANDIR',
'GOTONG-ROYONG','TEKA-TEKI','LAKI-LAKI','ANAK-ANAK','SAYUR-MAYUR','BUAH-BUAHAN','PURA-PURA'] # berisi nama nama ulangan

kata5=['FIKRI','ABYAN','JESSICA','SATRIA','NAJMI','ADITYA','NOVAL','IWAN','JERICHO',
'HILMI','FAJAR','FELISHA','FRENDY','ARSA','SOFYAN','KINANTI','ISNAINI','RIFDI','ROMZI',
'HAFIZH','AULIA','DESNANDA','CHRISTINA','RAYHAN','ARDHAN','RAFLI','IBRAHIM','SEBASTIAN',
'VIERI','CEAVIN','RYAAS','JAZMY','JOLI','FARREL','IVANA','FARHAN','THAQIF','DILLON',
'IVAN','JOHAN','ARIF','SERENA','ARYA','ARRIFQI','DIMAS','RAIHAN','WISNU'] # berisi nama teman satu kelas
